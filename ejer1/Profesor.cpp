#include <list>
#include <iostream>
using namespace std;
#include "Profesor.h"

int edad;
string nombre;
string sexo;

Profesor::Profesor(){}
Profesor::Profesor(int edad, string nombre, string sexo){
	this->edad = edad;
	this->nombre = nombre;
	this->sexo = sexo;
}
//setter & getter::
void Profesor::set_edad(int edad){
	this->edad = edad;
}
void Profesor::set_nombre(string nombre){
	this->nombre = nombre;
}
void Profesor::set_sexo(string sexo){
	this->sexo = sexo;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int Profesor::get_edad(){
	return this-> edad;
}
string Profesor::get_nombre(){
	return this->nombre;
}
string Profesor::get_sexo(){
	return this->sexo;
}
