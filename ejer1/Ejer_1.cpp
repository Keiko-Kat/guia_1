#include <list>
#include <iostream>
using namespace std;
#include "Profesor.cpp"

float Promedio_profe(Profesor array[], int N){
	int sum = 0, i=0;
	float out;
	for(i = 0; i<N; i++){
		sum = sum + array[i].get_edad();
	}
	out = sum / N;
	return out;
}

string Profe_menor(Profesor array[], int N){
	int i=0, menor = 0;
	//se recorre el arreglo
	for (i = 0; i < N; i++){
		//se revisa la condicion
		if (array[i].get_edad() < array[menor].get_edad()){
			menor = i;
		}
	}
	return array[menor].get_nombre();
}

string Profe_mayor(Profesor array[], int N){
	int i=0, mayor = 0;
	//se recorre el arreglo
	for (i = 0; i > N; i++){
		//se revisa la condicion
		if (array[i].get_edad() < array[mayor].get_edad()){
			mayor = i;
		}
	}
	return array[mayor].get_nombre();
}

int Profe_mas_prom(Profesor array[], int N, int prom){
	int i=0, mayor = 0;
	string gender = "mujer";
	//se recorre el arreglo
	for(i = 0; i < N; i++){
		//se revisa si dicho profesor es profesora o no
		if (array[i].get_sexo() == gender){
			//se confirma la ultima condicion
			if (array[i].get_edad() > prom){
				mayor++;
			}
		}
	}
	return mayor;
}

int Profe_min_prom(Profesor array[], int N, int prom){
	int i=0, menor = 0;
	string gender = "hombre";
	//se recorre el arreglo
	for(i = 0; i < N; i++){
		//se revisa si dicho profesor es profesor o no
		if (array[i].get_sexo() == gender){
			//se confirma la ultima condicion
			if (array[i].get_edad() < prom){
				menor++;
			}
		}
	}
	return menor;
}
	
		

int main(){
	string in, mayor, menor;
	int quantity, i=0, temp, prom, cant_mayor, cant_menor;
	
	cout<<"Ingrese tamaño del arreglo: ";
	getline(cin, in);
	quantity = stoi(in);
	if(quantity <= 0){
		cout<<"Cantidad invalida, ingrese de nuevo"<<endl;
		cout<<"Tamaño: ";
		getline(cin, in);
		quantity = stoi(in);
	}
	
	Profesor array[quantity];
	
	for (i = 0; i < quantity; i++){
		cout<<"Ingrese nombre del(la) profesor/a n°["<<i+1<<"]"<<endl;
		cout<<"~~~~> ";
		getline(cin, in);
		array[i].set_nombre(in);
		cout<<endl;
		cout<<"Ingrese edad del(la) profesor/a n°["<<i+1<<"]"<<endl;
		cout<<"~~~~> ";
		getline(cin, in);
		temp = stoi(in);
		array[i].set_edad(temp);
		cout<<endl;
		cout<<"Ingrese sexo del(la) profesor/a n°["<<i+1<<"]"<<endl;
		cout<<"~~~~> ";
		getline(cin, in);
		array[i].set_sexo(in);
		if(in != "hombre" && in != "mujer"){
			cout<<"Ingreso invalido, ingrese de nuevo"<<endl;
			cout<<"~~~~>";
			getline(cin, in);
			array[i].set_sexo(in);
		}		
	}
	prom = Promedio_profe(array, quantity);
	mayor = Profe_mayor(array, quantity);
	menor = Profe_menor(array, quantity);
	cant_mayor = Profe_mas_prom(array, quantity, prom);
	cant_menor = Profe_min_prom(array, quantity, prom);
	
	cout<<"La edad promedio de los profesores es: "<< prom<<endl;
	cout<<"El/la mayor de los profesores se llama: '"<<mayor<<"'"<<endl;
	cout<<"El/la menor de los profesores se llama: '"<<menor<<"'"<<endl;
	cout<<"Hay "<<cant_mayor<<" profesoras mayores que el promedio"<<endl;
	cout<<"Hay "<<cant_menor<<" profesores menores que el promedio"<<endl;
	
	cout<<endl;
	
	return 0;
}
