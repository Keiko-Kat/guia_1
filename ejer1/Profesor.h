#include <list>
#include <iostream>
using namespace std;

class Profesor{
	private:
	//los atributos privados::
		int edad;
		string nombre;
		string sexo;
	public:
		//constructores::
		Profesor();
		Profesor(int edad, string nombre, string sexo);
		//setter & getter::
		void set_edad(int edad);
		void set_nombre(string nombre);
		void set_sexo(string sexo);
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		int get_edad();
		string get_nombre();
		string get_sexo();
};
